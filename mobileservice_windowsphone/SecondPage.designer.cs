// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace mobileservice_windowsphone
{
	[Register ("SecondPage")]
	partial class SecondPage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SecondViewController btnGoback { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnGoback != null) {
				btnGoback.Dispose ();
				btnGoback = null;
			}
		}
	}
}
